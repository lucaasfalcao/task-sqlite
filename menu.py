import sqlite3
from uuid import uuid4
from reading import read_data
from register import register_data, change_data, delete_data

connection = sqlite3.connect("./mydatabase.db")
client = connection.cursor()

def database_menu():
    exit = False

    while not exit:

        action = input("Escolha entre CADASTRAR, LER ou SAIR: ")
        print()

        if action == "CADASTRAR":
            print("Agora, escolha o número referente à ação desejada:")
            print("1. Cadastrar um novo aluno.")
            print("2. Cadastrar um novo curso.")
            print("3. Cadastrar um novo professor.")
            print("4. Cadastrar uma nova disciplina.")
            print("5. Cadastrar um aluno em uma disciplina.")
            print("6. Alterar o curso de um aluno.")
            print("7. Alterar o professor responsável pela disciplina.")
            print("8. Remover a matrícula de um aluno em uma disciplina.")
            print()
            answer = int(input("Número: "))
            print()

            if answer == 1:
                register_data.register_student()
            elif answer == 2:
                register_data.register_course()
            elif answer == 3:
                register_data.register_teacher()
            elif answer == 4:
                register_data.register_discipline()
            elif answer == 5:
                register_data.register_student_into_discipline()
            elif answer == 6:
                change_data.change_student_course()
            elif answer == 7:
                change_data.change_discipline_teacher()
            elif answer == 8:
                delete_data.delete_enrollment_discipline()
            else:
                print("Erro: ação não encontrada.")
            print()

        elif action == "LER":
            print("Agora, escolha o número referente à ação desejada:")
            print("1. Ler todos os alunos cadastrados na faculdade.")
            print("2. Ler o curso associado e as disciplinas matriculadas de um aluno.")
            print("3. Ler a lista de alunos associados a um curso.")
            print("4. Ler a lista de alunos matriculados a uma disciplina.")
            print("5. Ler o ranking de disciplinas pelo número de alunos matriculados.")
            print()
            answer = int(input("Número: "))
            print()

            if answer == 1:
                read_data.all_students()
            elif answer == 2:
                read_data.a_student()
            elif answer == 3:
                read_data.course_students_list()
            elif answer == 4:
                read_data.discipline_students_list()
            elif answer == 5:
                read_data.ranking_disciplines()
            else:
                print("Erro: ação não encontrada.")
            print()

        elif action == "SAIR":
            print("Sistema fechado!")
            connection.close()
            exit = True

        else:
            print("Erro: ação não encontrada.")
