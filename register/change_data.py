import sqlite3

connection = sqlite3.connect("./mydatabase.db")
client = connection.cursor()

def change_student_course():
    student_id = input("ID do aluno: ")
    new_course_id = input("ID do novo curso: ")

    client.execute(
        f'UPDATE alunos SET curso_id = "{new_course_id}" WHERE id = "{student_id}";')
    connection.commit()
    
def change_discipline_teacher():
    discipline_id = input("ID da disciplina: ")
    new_teacher_id = input("ID do novo professor: ")

    client.execute(
        f'UPDATE disciplinas SET professor = "{new_teacher_id}" WHERE id = "{discipline_id}";')
    connection.commit()
