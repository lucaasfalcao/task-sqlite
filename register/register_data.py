import sqlite3
from uuid import uuid4

connection = sqlite3.connect("./mydatabase.db")
client = connection.cursor()

def register_student():
    cpf = input("CPF do aluno: ")
    name = input("Nome do aluno: ")
    birthday = input("Data de aniversário: ")
    rg = input("RG: ") or None
    dispatching_agency = input("Orgão expedidor: ") or None
    course_id = input("ID do curso: ")

    student_id = str(uuid4())

    db_rg = f'"{rg}"' if rg is not None else "NULL"
    db_dispatching_agency = f'"{dispatching_agency}"' if dispatching_agency is not None else "NULL"

    client.execute(
        f'INSERT INTO alunos values("{student_id}", "{cpf}", "{name}", "{birthday}", {db_rg}, {db_dispatching_agency}, "{course_id}");')
    connection.commit()

def register_course():
    name = input("Nome do curso: ")
    creation_year = int(input("Ano de criação: "))
    coordinator = input("Coordenador: ")
    building = input("Prédio: ")

    course_id = str(uuid4())

    client.execute(
        f'INSERT INTO cursos values("{course_id}", "{name}", "{creation_year}", "{coordinator}", "{building}");')
    connection.commit()

def register_teacher():
    cpf = input("CPF do professor: ")
    name = input("Nome do professor: ")
    titration = input("Titulação: ")

    teacher_id = str(uuid4())

    client.execute(
        f'INSERT INTO professores values("{teacher_id}", "{cpf}", "{name}", "{titration}");')
    connection.commit()

def register_discipline():
    name = input("Nome da disciplina: ")
    teacher_id = input("ID do professor da disciplina: ")
    description = input("Descrição da disciplina: ")
    code = input("Código da disciplina: ")

    discipline_id = str(uuid4())

    db_description = f'"{description}"' if description is not None else "NULL"

    client.execute(
        f'INSERT INTO disciplinas values("{discipline_id}", "{name}", "{teacher_id}", {db_description}, "{code}");')
    connection.commit()

def register_student_into_discipline():
    student_id = input("ID do aluno: ")
    discipline_id = input("ID da disciplina: ")

    client.execute(
        f'INSERT INTO aluno_disciplina values("{discipline_id}", "{student_id}");')
    connection.commit()
