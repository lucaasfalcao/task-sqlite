import sqlite3

connection = sqlite3.connect("./mydatabase.db")
client = connection.cursor()

def all_students():
    students = client.execute("SELECT * FROM alunos;")
    print("Dados de todos os alunos cadastrados na faculdade:")
    for student in students:
        print(student)

def a_student():
    student_id = input("ID do aluno: ")

    student_data = client.execute(
        f'SELECT alunos.curso_id, aluno_disciplina.disciplina_id FROM alunos JOIN aluno_disciplina ON alunos.id = aluno_disciplina.aluno_id and alunos.id = "{student_id}"')
    print()
    print("ID do curso e das disciplinas matriculadas, respectivamente:")
    for student in student_data:
        print(student)


def course_students_list():
    course_id = input("ID do curso: ")

    students_list = client.execute(
        f'SELECT alunos.id FROM alunos JOIN cursos ON alunos.curso_id = cursos.id and alunos.curso_id = "{course_id}"')
    print()
    print("ID dos alunos associados ao curso:")
    for student in students_list:
        print(student)


def discipline_students_list():
    discipline_id = input("ID da disciplina: ")

    students_list = client.execute(
        f'SELECT aluno_disciplina.aluno_id FROM aluno_disciplina JOIN disciplinas ON aluno_disciplina.disciplina_id = disciplinas.id and aluno_disciplina.disciplina_id = "{discipline_id}"')
    print()
    print("ID dos alunos matriculados à disciplina:")
    for student in students_list:
        print(student)


def ranking_disciplines():
    ranking_list = []
    disciplines_number = int(input("Digite o número total de disciplinas: "))
    print()
    print("Por favor, insira o ID das disciplinas abaixo:")
    for i in range (disciplines_number):
        discipline_id = input(f'ID da {i+1}° disciplina: ')
        ranking = client.execute(
            f'SELECT disciplina_id, COUNT(disciplina_id) FROM aluno_disciplina WHERE disciplina_id = "{discipline_id}"')
        for rank in ranking:
            ranking_list.append(rank)
    print()
    print("ID da disciplina e quantidade de alunos matriculados, respectivamente:")
    print(ranking_list)
